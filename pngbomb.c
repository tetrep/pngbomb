#include <stdio.h>
#include <stdlib.h>
#include <png.h>
#include <zlib.h> // so we can use Z_BEST_COMPRESSION

int main (int argc, char *argv[]) {
  // make sure we have dimensons
  if (3 != argc) {
    fprintf(stderr, "Usage: %s [width] [height]\n", argv[0]);
    exit(1);
  }

  png_structp pp = NULL;
  png_infop ip = NULL;
  png_bytep bp = NULL;

  size_t height = atoi(argv[1]);
  size_t width = atoi(argv[2]);

  if (NULL == (pp = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL))) {
    fprintf(stderr, "could not allocate write struct\n");
  } else if (NULL == (ip = png_create_info_struct(pp))) {
    fprintf(stderr, "could not allocate info struct\n");
  } else if (setjmp(png_jmpbuf(pp))) {
    fprintf(stderr, "png exception!\n");
    exit(1);
  }

  png_init_io(pp, stdout);
  png_set_user_limits(pp, (1 << 31) - 1, (1 << 31) - 1);
  png_set_IHDR(pp, ip, width, height, 1, PNG_COLOR_TYPE_GRAY, PNG_INTERLACE_NONE,
               PNG_COMPRESSION_TYPE_DEFAULT, PNG_FILTER_TYPE_DEFAULT);
  //png_set_filter(pp, 0, PNG_ALL_FILTERS);

  // play with zlib settings
  png_set_compression_level(pp, Z_BEST_COMPRESSION);
  png_set_compression_mem_level(pp, 9);
  //png_set_compression_buffer_size(pp, 32000);
  //png_set_compression_window_bits(pp, 16);

  png_write_info(pp, ip);

  // idk shit about how much memory to allocate
  if (NULL == (bp = calloc(width, 3 * sizeof(png_byte)))) {
    fprintf(stderr, "not enough memory for row\n");
    exit(2);
  }
  for (size_t x = 0; x < height; x++) {
    png_write_row(pp, bp);
    if (0 == (x % 10000)) {
      fprintf(stderr, "wrote row %lu\n", x);
    }
  }
  png_write_end(pp, NULL);
  

  return 0;
}
