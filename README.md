# pngbomb
## what do
Creates compression bombs with libpng. The run script will default to generating a 65536x65536 PNG, if just the width is specified a square will be generated, otherwise the specified width and height will be used to create the PNG.

## usage
The PNG is written to stdout.
./pngbomb.exe [width] [height]
