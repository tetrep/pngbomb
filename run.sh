#!/bin/bash
WIDTH=${1}
HEIGHT=${2}
if [ -z "${1}" ]; then
  WIDTH=65536
fi
if [ -z "${2}" ]; then
  HEIGHT=${WIDTH}
fi
./build.sh &&
./pngbomb.exe ${WIDTH} ${HEIGHT} > "${WIDTH}x${HEIGHT}.png"
